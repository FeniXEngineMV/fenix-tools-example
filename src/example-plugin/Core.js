/**
 * The plugins Core file, which contains registration to FeniXCore and export(s) of it's
 * important members.
 *
 * @file Core
 *
 * @author       FeniXEngine Contributers
 * @copyright    2018 FeniX Engine
 * @license      MIT
 */

 import {loadLocalFile, hasWebAccess, convertParameters} from 'fenix-tools'

console.log(loadLocalFile)
console.log(hasWebAccess)
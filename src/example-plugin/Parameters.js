/**
 * All plugin parameters for this plugin
 *
 * @file parameters
 *
 * @author       FeniXEngine Contributers
 * @copyright    2018 FeniX Engine
 * @license      MIT
 */

/*:
 * @plugindesc The core plugin for FeniXEngine which provides a robust API for plugin developers.
 *
 * @author FeniX Contributers (https://fenixenginemv.gitlab.io/)
 *
 * @help
--------------------------------------------------------------------------------
 # TERMS OF USE

 -------------------------------------------------------------------------------
 # INFORMATION
*/

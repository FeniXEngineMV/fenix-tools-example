#  fenix-tools-example

<a href='https://ko-fi.com/ltngames' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://az743702.vo.msecnd.net/cdn/kofi2.png?v=0' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a>

FeniXTools is a modular library built for use with RPG Maker MV plugin development and this is an example
to demonstrate how to use FeniXTools in your own projects.

## Pre-Installation

 **Note:** _Skip if you already have NodeJS & npm._

#### Windows

- Install [NodeJS](https://nodejs.org/en/)

#### Ubuntu

- Install [NodeJS](https://nodejs.org/en/)

```
sudo apt-get install nodejs
```

- Install [npm](https://www.npmjs.com/)

```
sudo apt-get install npm
```

# Getting Started

If you would rather an easy to use command line tool, then check out
*[FeniXCLI](https://gitlab.com/FeniXEngineMV/fenix-cli), a CLI tool that generates and builds projects for you*

### Clone fenix-tools-example

```
git clone git@gitlab.com:FeniXEngineMV/fenix-tools-example.git
```

### Install Packages

```
npm install
```

### Build the example plugin

```
npm run build
```

### Play time!

The best way to learn is to learn hands-on so learn how this example works, change things around, include diffrent methods from fenix-tools and have fun!

# Documentation

A good place to get started for FeniXTools, is the documentation page for a list of available methods.

[Documentation](https://fenixenginemv.gitlab.io/fenix-tools/)

# Contributing
Please read over the [Contribution Guide](https://gitlab.com/FeniXEngineMV/fenix-tools/blob/dev/CONTRIBUTING.md)

# License
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[The MIT License](https://gitlab.com/FeniXEngineMV/fenix-tools/blob/dev/LICENSE.md)
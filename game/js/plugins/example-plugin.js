(function () {
'use strict';

/**
 * All plugin parameters for this plugin
 *
 * @file parameters
 *
 * @author       FeniXEngine Contributers
 * @copyright    2018 FeniX Engine
 * @license      MIT
 */

/*:
 * @plugindesc The core plugin for FeniXEngine which provides a robust API for plugin developers.
 *
 * @author FeniX Contributers (https://fenixenginemv.gitlab.io/)
 *
 * @help
--------------------------------------------------------------------------------
 # TERMS OF USE

 -------------------------------------------------------------------------------
 # INFORMATION
*/

/**
 * An async function which determines if the current computer has internet
 * access by pinging to a server. If no url is provided it checks
 * navigator.online
 *
 * @function hasWebAccess
 * @async
 * @since 1.0.0
 * @memberof module:File
 *
 * @param {string} [url] A url to a server
 *
 * @return {promise} A promise that resolves to true or false
 * @example
 * import { hasWebAccess } from 'fenix-tools'
 *
 * const canDownload = await hasWebAccess('http://google.com')
 * console.log(canDownload) // => returns true
 *
 * hasWebAccess('http://google.com')
 * .then('Web connection is live!)
 */
async function hasWebAccess (url) {
  if (!url) { return navigator.onLine }
  const result = await window.fetch(url, {
    method: 'HEAD',
    cache: 'no-cache'
  }).then((response) => {
    return response.ok === true
  });
  return result
}

/**
 * Load a file on the local machine.
 *
 * @function loadLocalFile
 * @since 1.0.0
 * @memberof module:File
 *
 * @param {string} path - Path to the file.
 * @param {string} [encoding='utf8'] - The type of file encoding
 *
 * @return {promise} A promise that resolves the data
 * @example
 * import {loadLocalFile} from 'fenix-tools'
 *
 * loadLocalFile('./img/pictures/character1.png)
 * .then(data => {
 *  // Local file loaded success
 *  console.log(data)  // => A parsed JSON object.
 * })
 *
 */
function loadLocalFile (path, encoding = 'utf8') {
  const fs = require('fs');
  return new Promise((resolve, reject) => {
    if (fs.existsSync(path)) {
      const contents = fs.readFileSync(path, encoding, err => reject(err));
      resolve(contents);
    }
    reject(new Error(`Cannot read file at ${path}`));
  })
}

/**
 * The plugins Core file, which contains registration to FeniXCore and export(s) of it's
 * important members.
 *
 * @file Core
 *
 * @author       FeniXEngine Contributers
 * @copyright    2018 FeniX Engine
 * @license      MIT
 */

console.log(loadLocalFile);
console.log(hasWebAccess);

/* eslint-disable */

}());

import resolve from 'rollup-plugin-node-resolve'

export default {
  input: `./src/example-plugin/main.js`,
  external: [
    'fs-extra',
    'http',
    'path'
  ],
  output: [{
    file: `./game/js/plugins/example-plugin.js`,
    format: 'iife',
    indent: false
  }
  ],
  plugins: [
    resolve({
      jsnext: true,
      module: true
    }
    )
  ]
}
